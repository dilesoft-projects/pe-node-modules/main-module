import collection from './collection';
import collectionItem from './collectionItem';
import configCollection from './configCollection';
import serverConfig from './serverConfig';
import remoteAuth from './remoteAuth';

// module.exports = {
//
//         collection: collection,
//         collectionItem: collectionItem,
//         configCollection: configCollection,
//
// }

export default function (serverActor) {
  const collectionActor = collection(serverActor);
  const collectionItemActor = collectionItem(serverActor);
  const configCollectionActor = configCollection(serverActor);
  const serverConfigActor = serverConfig(serverActor);
  const remoteAuthActor = remoteAuth(serverActor);
}
